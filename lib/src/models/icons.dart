import 'package:flutter/material.dart';

class Option {
  Icon icon;
  String title;
  String subtitle;

  Option({this.icon, this.title, this.subtitle});
}

final options = [
  Option(
    icon: Icon(Icons.add_a_photo, size: 40.0),
    title: 'Photo!',
    subtitle: 'Texto de prueba de la sesion 12 usnaod Flutter',
  ),
  Option(
    icon: Icon(Icons.add_call, size: 40.0),
    title: 'Call',
    subtitle: 'Texto de prueba de la sesion 12 usnaod Flutter',
  ),
  Option(
    icon: Icon(Icons.add_location, size: 40.0),
    title: 'GPS',
    subtitle: 'Texto de prueba de la sesion 12 usnaod Flutter',
  ),
  Option(
    icon: Icon(Icons.airplanemode_inactive, size: 40.0),
    title: 'Airplane',
    subtitle: 'Texto de prueba de la sesion 12 usnaod Flutter',
  ),
  Option(
    icon: Icon(Icons.alarm_on, size: 40.0),
    title: 'Alarm',
    subtitle: 'Texto de prueba de la sesion 12 usnaod Flutter',
  ),
  Option(
    icon: Icon(Icons.android, size: 40.0),
    title: 'Android',
    subtitle: 'Texto de prueba de la sesion 12 usnaod Flutter',
  ),
  Option(
    icon: Icon(Icons.add_circle, size: 40.0),
    title: 'More',
    subtitle: 'Texto de prueba de la sesion 12 usnaod Flutter.',
  ),
  Option(
    icon: Icon(Icons.add_shopping_cart, size: 40.0),
    title: 'Shopping',
    subtitle: 'Texto de prueba de la sesion 12 usnaod Flutter.',
  ),
];
