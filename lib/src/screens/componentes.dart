import 'package:flutter/material.dart';
import 'package:sesion11/src/models/icons.dart';

class Componentes extends StatefulWidget {
  @override
  _ComponentesState createState() => _ComponentesState();
}

class _ComponentesState extends State<Componentes> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepOrange,
        title: Text('Componentes', style: TextStyle(fontSize: 30),),
      ),
      body: ListView.builder(
        itemCount: options.length,
        itemBuilder: (BuildContext context, int index) {
          return Container(
            //Container de cada Icon
            alignment: Alignment.center,
            margin: EdgeInsets.all(5.0),
            width: double.infinity,
            height: 95.0,
            decoration: BoxDecoration(
              color: Colors.limeAccent[100],
              borderRadius: BorderRadius.circular(10.0),
              border: Border.all(color: Colors.black26),
            ),
            //Title de cada opcion
            child: ListTile(
              leading: options[index].icon,
              title: Text(
                options[index].title,
                style: TextStyle(
                  color: Colors.black,
                ),
              ),
              //Subtitulo de cada opcion
              subtitle: Text(
                options[index].subtitle,
                style: TextStyle(
                  color: Colors.black
                ),
              ),              
            ),
          );
        },
      ),
    );
  }
}
