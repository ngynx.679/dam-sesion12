import 'package:flutter/material.dart';
import 'package:sesion11/src/pages/home_page.dart';
import 'package:sesion11/src/pages/contador_page.dart';

import 'package:sesion11/src/screens/componentes.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Center(
        child: Componentes(),
      ),
    );
  }
}